<?php
function usertheme($codeName){
    switch ($codeName){
        case "header" :
            $tempid = (strstr($_SERVER["PHP_SELF"],"index")) ? 'id="selected"' : "";
            return <<<START_TEXT
                <html>
                <head>
                    <meta charset = utf-8 >
                    <title><?php echo isset($setAbout["title"]) ? $setAbout["title"] : \'Simcrip Uploader\'; ?></title>
                    <meta name="description" content="<?php echo isset($setAbout["description"]) ? $setAbout["description"] : \'Simcrip Uploader is a free script for upload center sites\'; ?>" />
                    <meta name="keywords" content="<?php echo $setAbout["tags"] ; ?>" />
                    <meta name="author" content="<?php echo $setAbout["adminName"] ; ?>" /> 
                    <meta http-equiv="Designer" content="Brackets">
                    <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
                    <meta name="copyright" content="Built-in time 2014-03-26 15:09:53 | website : http://alia.cf" />
                    <meta name="robots" content="index, follow" />

                    <link rel="stylesheet" type="text/css" href="./theme/Style/main.css">
                    <script lang="javascript" type="text/javascript" src="./theme/js/dragdropUpload.js"></script>
                </head>

                <body>
                <Section id="main">

                    <header>

                        <nav>
                            <ul>
                                <li><a href="#" <?php echo (strstr($_SERVER["PHP_SELF"],"index")) ? \'id="selected"\' : "";?>> Home </a></li>
                                <li><a href="#" <?php echo (strstr($_SERVER["PHP_SELF"],"contact")) ? \'id="selected"\' : "";?>> Contact </a></li>
                                <li><a href="#" <?php echo (strstr($_SERVER["PHP_SELF"],"about")) ? \'id="selected"\' : "";?>> About </a></li>
                            </ul>
                        </nav>
                        <?php 
                            if (isset($setAbout["notification"]) && !empty($setAbout["notification"])){
                                echo \'<article id="notifications"><p>\' . $setAbout["notification"] . \'</p></article>\';
                            } else {
                                echo "";
                            }
                        ?>
                    </header>';
                START_TEXT;
        break;
    }
}
 
