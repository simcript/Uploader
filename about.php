<?php
include('./config.inc.php');
?>
<!DOCTYPE HTML>
<html>
<head>
        <meta charset = utf-8 >
        <title><?php echo isset($setAbout["title"]) ? $setAbout["title"] : 'Simcrip Uploader'; ?></title>
        <meta name="description" content="<?php echo isset($setAbout["description"]) ? $setAbout["description"] : 'Simcrip Uploader is a free script for upload center sites'; ?>" />
        <meta name="keywords" content="<?php echo $setAbout["tags"] ; ?>" />
        <meta name="author" content="<?php echo $setAbout["adminName"] ; ?>" /> 
        <meta http-equiv="Designer" content="Brackets">
        <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
        <meta name="copyright" content="Built-in time 2014-03-26 15:09:53 | website : http://alia.cf" />
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" type="text/css" href="./theme/Style/main.css">
        <script lang="javascript" type="text/javascript" src="./theme/js/dragdropUpload.js"></script>
</head>

<body>
    <Section id="main">

        <header>

            <nav>
                <ul>
                    <li><a href="./index.php" <?php echo (strstr($_SERVER['PHP_SELF'],'index')) ? 'id="selected"' : '';?>> Home </a></li>
                    <li><a href="./contact.php" <?php echo (strstr($_SERVER['PHP_SELF'],'contact')) ? 'id="selected"' : '';?>> Contact </a></li>
                    <li><a href="./about.php" <?php echo (strstr($_SERVER['PHP_SELF'],'about')) ? 'id="selected"' : '';?>> About </a></li>
                </ul>
            </nav>
            <?php 
                if (isset($setAbout["notification"]) && !empty($setAbout["notification"])){
                    echo '<article id="notifications"><p>' . $setAbout["notification"] . '</p></article>';
                } else {
                    echo '';
                }
            ?>
        </header>
        <article id="about">
            <p>
            <?php 
                if (empty($setAbout["about"])) { 
                    echo 'Oh sorry! <br> This page is not available';
                } else { 
                    echo $setAbout["about"];
            } ?>
            </p>
        </article>
        <footer>
            <p><?php 
                    $startYear = explode("-",$setAbout["created"]);
                    echo (date('Y') == $startYear[0]) ? $startYear[0] : $startYear[0] . '-' . date('y');
                    echo ' ';
                    echo empty($setAbout["copyRight"]) ? '&copy; All rights reserved.' : $setAbout["copyRight"] ; 
                ?> </p>
        </footer>
    </Section><!--main-->
</body>
</html>