<?php
include('./config.inc.php');
if (isset($_POST['flag'])){
    require_once('./Classes.php');
    try {
        $upload = new SS_Upload($destination);
        $upload->setPermittedTypes($setAbout["mime"]);
        $upload->move();
        $result = $upload->getMessages();
        $i=0;
        foreach ($upload->getFilenames() as $fileName) {
            $fileCode = pCodeGenarate();
            if (!saveDetails($fileName, getMIME($fileName), $upload->getFileSize($fileName), $fileCode, GetRealIp(), getLinks($fileName), fileLocation($fileName,$destination))){
                $errors[] = 'Connect to data base failed!';
            } else {
                $i++;
                $outPuts[$i] = outPutScript($fileName);
            }
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    
}


function saveDetails($fname, $fmime, $fsize, $pCode, $fIp, $fdlLink, $flocation) {
    $conn = dbConnect();
    $sql ='INSERT INTO file_detiles (file_name, mime, size, proprietary_code, uploaded, ip_uploader,  download_link, file_location)
                  VALUES (?, ?, ?, ?, NOW(), ?, ?, ?)';
    $stmt = $conn->stmt_init();
    if ($stmt->prepare($sql)) {
        $stmt->bind_param('ssissss', $fname, $fmime, $fsize, $pCode, $fIp, $fdlLink, $flocation);
        if ($stmt->execute()){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function pCodeGenarate(){
    return md5(uniqid(rand()));
}

function getMIME($name) {
    $dot = strrpos($name, '.');
    if ($dot) {
        $extension = substr($name, $dot);
    } else {
        $extension = 'NoType';
    }
    return $extension;
}

function fileLocation($name,$tmpDestination){
    $tmpDestination = str_replace('.', '', $tmpDestination);
    $uploadsDirectory = $_SERVER['PHP_SELF'] . $tmpDestination;
    $uploadsDirectory = str_replace('/index.php', '', $uploadsDirectory);
    $filePath = $_SERVER['SERVER_NAME'] . $uploadsDirectory . $name;
    return $filePath;
}

function outPutScript($fileName){
    $conn = dbConnect();
    $sql = 'SELECT DISTINCT size, proprietary_code, download_link FROM file_detiles WHERE file_name = ?';
    $stmt = $conn->stmt_init();
    if ($stmt->prepare($sql)) {
        $stmt->bind_param('s', $fileName);
        $OK = $stmt->execute();
        $stmt->bind_result($outPut["size"], $outPut["dell"], $outPut["link"]);
        $stmt->fetch();
        if ($OK){
            $outPut["name"] = $fileName;                                            //name file uploaded
            $outPut["size"] = $outPut["size"] / 1024;                               //converter byte to KiloByte
            $outPut["size"] = number_format($outPut["size"], 2);                    //size file uploaded
            $outPut["info"] = getLinks($fileName,'/info.php?fn=');                  //information of file uploaded
            $outPut["link"] = $outPut["link"];                                      //direct link for download file uploaded
            $outPut["dell"] = getLinks($outPut["dell"],'/dl.php?pc=');              //delete link for delete file uploaded
            return $outPut;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
?>