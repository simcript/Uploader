<?php
    include('./config.inc.php');
    
    if (isset($_GET['fn'])) {
            $conn = dbConnect();
            $stmt = $conn->stmt_init();
            $sql = 'SELECT file_name, mime, size, proprietary_code, uploaded, ip_uploader, total_download, last_downloader, download_link, file_location, deleted, ip_deleter FROM file_detiles WHERE file_name = ?';
            if ($stmt->prepare($sql)) {
                $stmt->bind_param('s', $_GET['fn']);
                $stmt->bind_result($fname, $ftype, $fSize, $fPC, $upDate, $ipUp, $totalDL, $lastDL, $linkDL, $locationFile, $delDate, $ipDel);
                if ($stmt->execute() && $stmt->fetch()) {
                    $OK = TRUE;
                } else {
                    $OK = False;
                }
            }
        } else {
            header("Location: ./trash.php");
        }
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset = utf-8 >
        <title><?php echo isset($setAbout["title"]) ? $setAbout["title"] : 'Simcrip Uploader'; ?></title>
        <meta name="description" content="<?php echo isset($setAbout["description"]) ? $setAbout["description"] : 'Simcrip Uploader is a free script for upload center sites'; ?>" />
        <meta name="keywords" content="<?php echo $setAbout["tags"] ; ?>" />
        <meta name="author" content="<?php echo $setAbout["adminName"] ; ?>" /> 
        <meta http-equiv="Designer" content="Brackets">
        <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
        <meta name="copyright" content="Built-in time 2014-03-26 15:09:53 | website : http://alia.cf" />
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" type="text/css" href="./theme/Style/main.css">
        <script lang="javascript" type="text/javascript" src="./theme/js/dragdropUpload.js"></script>
    </head>

    <body>
    <Section id="main">

        <header>

            <nav>
                <ul>
                    <li><a href="./index.php" <?php echo (strstr($_SERVER['PHP_SELF'],'index')) ? 'id="selected"' : '';?>> Home </a></li>
                    <li><a href="./contact.php" <?php echo (strstr($_SERVER['PHP_SELF'],'contact')) ? 'id="selected"' : '';?>> Contact </a></li>
                    <li><a href="./about.php" <?php echo (strstr($_SERVER['PHP_SELF'],'about')) ? 'id="selected"' : '';?>> About </a></li>
                </ul>
            </nav>
            <?php 
                if (isset($setAbout["notification"]) && !empty($setAbout["notification"])){
                    echo '<article id="notifications"><p>' . $setAbout["notification"] . '</p></article>';
                } else {
                    echo '';
                }
            ?>
        </header>
        <section id="info">
            <div class="output">
                
                <?php
                    echo "file name : <h5> $fname</h5><br>";
                    echo "file MIME : <h5> $ftype</h5><br>";
                    echo "file Size : <h5> $fSize</h5><br>";
                    echo "Upload date : <h5> $upDate</h5><br>";
                    echo "Total Download : <h5> " ; echo !empty($totalDL) ?  $totalDL : '0' ; echo "</h5><br>";
                    echo !empty($delDate) ? "Delete date : <h5> $delDate</h5><br>" : '';
                ?>
            </div>
        </section>
        
        
        <footer>
            <p><?php 
                    $startYear = explode("-",$setAbout["created"]);
                    echo (date('Y') == $startYear[0]) ? $startYear[0] : $startYear[0] . '-' . date('y');
                    echo ' ';
                    echo empty($setAbout["copyRight"]) ? '&copy; All rights reserved.' : $setAbout["copyRight"] ; 
                ?> </p>
        </footer>
</Section><!--main-->
</body>
</html>