<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/23/2016
 * Time: 10:44 AM
 */
include('./config.inc.php');
$Ips = GetRealIp();
if (isset($_GET['fn'])) { //fn is Abbreviation file name code
    try {
        $conn = dbConnect();
        $sql = 'SELECT id_file, mime, size, total_download, download_link, deleted, ip_deleter FROM file_detiles WHERE file_name = ?';
        $stmt = $conn->stmt_init();
        $stmt->prepare($sql);
        $stmt->bind_param('s', $_GET['fn']);
        $stmt->bind_result($fileId, $fileType, $fileSize, $totalDl, $dLink, $is_deleted, $ip_del);
        $stmt->execute();
        $stmt->fetch();
        if (empty($is_deleted) && empty($ip_del)){
            $failName = $destination . $_GET['fn'];
            if (download_file($failName, $fileSize, $fileType)) {
                $totalDl++;

                $sql = 'INSERT INTO downloaders (id_file, ip_downloader, date_download) VALUES (?, ?, NOW())';
                $stmt = $conn->stmt_init();
                $stmt->prepare($sql);
                $stmt->bind_param('is', $fileId, $Ips);
                $stmt->execute();

                $sql = 'SELECT id_downloader FROM downloaders WHERE date_download = NOW() AND ip_downloader = ? AND id_file = ?';
                $stmt = $conn->stmt_init();
                $stmt->prepare($sql);
                $stmt->bind_param('si', $Ips, $fileId);
                $stmt->bind_result($downloaderId);
                $stmt->execute();
                $stmt->fetch();


                $sql = 'UPDATE file_detiles SET total_download = ?, last_downloader = ? WHERE file_name = ?';
                $stmt = $conn->stmt_init();
                $stmt->prepare($sql);
                $stmt->bind_param('iis', $totalDl, $downloaderId ,$_GET['fn']);
                $stmt->execute();
            } else {
                echo 'Download failed';
            }
        } else {
            echo 'This File deleted';
        }

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

if (isset($_GET['pc'])) { //pc is Abbreviation Proprietary code
    try {
        $conn = dbConnect();
        $sql = 'SELECT deleted, ip_deleter FROM file_detiles WHERE proprietary_code = ?';
        $stmt = $conn->stmt_init();
        $stmt->prepare($sql);
        $stmt->bind_param('s', $_GET['pc']);
        $stmt->bind_result($is_deleted, $ip_del);
        $stmt->execute();
        $stmt->fetch();
        if (empty($is_deleted) && empty($ip_del)) {
            $sql = 'UPDATE file_detiles SET deleted = NOW(), ip_deleter = ? WHERE proprietary_code = ?';
            $stmt = $conn->stmt_init();
            $stmt->prepare($sql);
            $stmt->bind_param('ss', $Ips, $_GET['pc']);
            $dele = $stmt->execute();
            if ($dele) {
                echo "File deleted successfully.";
            } else {
                echo 'Error';
            }
        } else {
            echo 'This File deleted';
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
function download_file($file, $fsize, $ftype){
    set_time_limit(-1);
    if(file_exists($file)){
        $fileName = explode('/',$file);
        $fileName = $fileName[count($fileName)-1];
        header('Content-Description: File Transfer');
        header('Content-Type: '.$ftype);
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: '.$fsize);
        ob_clean();
        flush();
        readfile("$file");
        return true;
    } else {
        return false;
    }
}

?>
