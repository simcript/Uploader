<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/24/2016
 * Time: 05:59 PM
 */
include('./config.inc.php');
require_once ('./admin/functions.inc.php');
if (isset($_GET['logout'])){
    if ($_GET['logout'] == 'yes') {
        session_start();
        logout('./index.php');
    }
} else {
    if (isset($_POST['login'])) {
        $username = trim($_POST['uname']);
        $password = $_POST['upass'];
        $redirect = './admin/index.php';
        $conn = dbConnect();
        $sql = 'SELECT password FROM configuration WHERE username = ?';
        $stmt = $conn->stmt_init();
        $stmt->prepare($sql);
        $stmt->bind_param('s', $username);
        $stmt->bind_result($storedPwd);
        $stmt->execute();
        $stmt->fetch();
        if (sha1($password) == $storedPwd) {
            session_start();
            $_SESSION['ssu'] = 'Saleh';
            // get the time the session started
            $_SESSION['start'] = time();
            session_regenerate_id();
            header("Location: $redirect");
            exit;
        } else {
            // if no match, prepare error message
            $error = 'Invalid username or password';
        }

    }
    sessionCheck(true, './index.php');
}
if (isset($_GET['forget'])) {
    $showFild = false;
    if (isset($_POST['resetpass'])){
        $email = $_POST['uemail'];
        
        $conn = dbConnect();
        $sql = 'SELECT site_title, username FROM configuration WHERE email = ?';
        $stmt = $conn->stmt_init();
        $stmt->prepare($sql);
        $stmt->bind_param('s', $email);
        $stmt->bind_result($storedSiteTitle, $storedUn);
        $stmt->execute();
        $stmt->fetch();
        if(strlen($storedUn)>1){
            $newPass = newPassword($email);
            $contentEmail = "Username =  $storedUn\r\n";
            $contentEmail .= "New Password =  $newPass\r\n";
            $contentEmail .= 'Please change your password immediately.';
            $m = mailer($email,'Forgot your username in' . $storedSiteTitle , $contentEmail, $storedSiteTitle, $email);
            if (!$m){
                $error = "Send e-mail having problems, please contact your host supports the site Simcrip report.";
            }
        } else {
            $error = 'E-mail address is wrong!';
        }
        
    }
} else {
    $showFild = true;
}
function newPassword($email){
    $newpass = rand(mt_rand(15493584,92414628),mt_rand(154963584,915414628));
    $conn = dbConnect();
    $sql = 'SELECT id_config FROM configuration WHERE email = ?';
    $stmt = $conn->stmt_init();
    $stmt->prepare($sql);
    $stmt->bind_param('s', $email);
    $stmt->bind_result($id_pass);
    $stmt->execute();
    $stmt->fetch();
    ///////////////////////////
    $stmt = $conn->stmt_init();
    $sql = 'UPDATE configuration SET password = ? WHERE email = ?';
    if ($stmt->prepare($sql)) {
        $encodeNewpass = sha1($newpass);
        $stmt->bind_param('si', $encodeNewpass, $email);
        $stmt->execute();
        return $newpass;
    } else {
        return 'Error! Do not generate NewPassword';
    }

    return 'Email not found';
}
?>
<!DOCTYPE HTML>
<html>
<head>
        <meta charset = utf-8 >
        <title><?php echo isset($setAbout["title"]) ? $setAbout["title"] : 'Simcrip Uploader'; ?></title>
        <meta name="description" content="<?php echo isset($setAbout["description"]) ? $setAbout["description"] : 'Simcrip Uploader is a free script for upload center sites'; ?>" />
        <meta name="keywords" content="<?php echo $setAbout["tags"] ; ?>" />
        <meta name="author" content="<?php echo $setAbout["adminName"] ; ?>" /> 
        <meta http-equiv="Designer" content="Brackets">
        <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
        <meta name="copyright" content="Built-in time 2014-03-26 15:09:53 | website : http://alia.cf" />
        <meta name="robots" content="noindex, nofollow" />

        <link rel="stylesheet" type="text/css" href="./theme/Style/main.css">
</head>

<body>
      
    
    <Section id="main">

        <header>

            <nav>
                <ul>
                    <li><a href="./index.php" <?php echo (strstr($_SERVER['PHP_SELF'],'index')) ? 'id="selected"' : '';?>> Home </a></li>
                    <li><a href="./contact.php" <?php echo (strstr($_SERVER['PHP_SELF'],'contact')) ? 'id="selected"' : '';?>> Contact </a></li>
                    <li><a href="./about.php" <?php echo (strstr($_SERVER['PHP_SELF'],'about')) ? 'id="selected"' : '';?>> About </a></li>
                </ul>
            </nav>
        </header>
        
        <section id="login">
            <?php if ($showFild) { ?>
                <form id="enteradmin" action="" method="post" >
                    <input type="text" class="typeText" id="youname" name="uname" maxlength="30" placeholder="Username">
                    <input  type="password" class="typeText" id="youpass" name="upass" maxlength="100" placeholder="Password">
                    <a href="./login.php?forget=yes" >Remember your password?</a>
                    <input type="submit" class="btnSubmit" id="enter" name="login" value="Login">
                </form>
            <?php } else { ?>
                <form id="forgetadmin" method="post" action="">
                    <input type="email" class="typeText" id="youemail" name="uemail" maxlength="100" placeholder="Email">
                    <a href="./login.php">You want to login?</a>
                    <input type="submit" class="btnSubmit" id="sendmail" name="resetpass" value="Reset">
                </form>    
                <!-- <?php #echo isset($contentEmail) ? $contentEmail : 'false'; //if mailer function Not implemented delete Sharpe character(#) of begin this line. Then enter email address again and click on Reset and find "New Password" at source page! BUT it is better to replace mailer function at directory "admin/functions.inc.php" ?> -->
            <?php } ?>
            
            <?php if (isset($error)){ ?>
            <section id="result">
                <?php
                         
                 echo "<p>Error: $error</p>";
                        
                ?>
            </section>
            <?php } ?>
        </section> <!-- formContact -->
        
        <footer>
            <p><?php 
                $startYear = explode("-",$setAbout["created"]);
                echo (date('Y') == $startYear[0]) ? $startYear[0] : $startYear[0] . '-' . date('y');
                echo ' ';
                echo empty($setAbout["copyRight"]) ? '&copy; All rights reserved.' : $setAbout["copyRight"] ; 
            ?> </p>
        </footer>
    </Section><!--main-->
</body>
</html>