<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/23/2016
 * Time: 10:48 AM
 */
if (!function_exists('dbConnect')){
 header('Location: ./Inestaller/');
}
$setAbout = getAboutSetting();
$maxSize = $setAbout["maxSize"]; // set maximum size for upload file default is 1048576 Byte or 1 MB
$destination = $setAbout["path"]; //default directory for uploaded files
define("maxLenContent", 50, true);  //maximum Length Content message in inbox.php file

function GetRealIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ip = $_SERVER['REMOTE_ADDR'];
    return (string) $ip;
}

function getAboutSetting(){
    $conn = dbConnect();
    $sql = "SELECT site_title, site_description, site_tag, site_copy, admin_name, site_created, site_about, notification, max_up_size, mime_allowed, upload_location, message_allowed FROM configuration";
    $stmt = $conn->stmt_init();
    if ($stmt->prepare($sql)) {
        $stmt->execute();
        $stmt->bind_result($setAbout["title"], $setAbout["description"], $setAbout["tags"], $setAbout["copyRight"], $setAbout["adminName"], $setAbout["created"], $setAbout["about"], $setAbout["notification"], $setAbout["maxSize"], $setAbout["mime"], $setAbout["path"], $setAbout["message"]);                 //  $setAbout = About Setting site
        if($stmt->fetch()){
            return $setAbout;
        }
    }
}

function getLinks($filename, $prefixFilename = '/dl.php?fn=', $suffixFilename = '', $typelink = 'directLink'){
    switch($typelink){
        case "directLink":
            $tmpProtcol = $_SERVER['SERVER_PROTOCOL'];
            $slash = strrpos($tmpProtcol,'/');
            if ($slash) {
                $tmpProtcol = strtolower (substr($tmpProtcol, 0, $slash)) . '://';
            }
            $baseDomin = $tmpProtcol . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
            $slash = strrpos($baseDomin,'/');
            if ($slash){
                $baseDomin =substr($baseDomin, 0, $slash);
                $baseDomin = $baseDomin . $prefixFilename . $filename . $suffixFilename;
            } else {
                $baseDomin = $baseDomin . $prefixFilename . $filename . $suffixFilename;
            }
            break;   
    }

    return isset($baseDomin) ? $baseDomin : 'NoLink';
}