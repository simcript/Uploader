<?php
require_once('./conf.inc.php');
if (isset($_POST['inestall'])) {
	if (file_exists($sqlFilePath)) {
		$sqlFile = fopen($sqlFilePath, 'r');
			$TempData = fread($sqlFile, filesize($sqlFilePath));
		fclose($sqlFile);
	}
	$TempData = explode("/*RULE_SQL_RULE*/", $TempData);

	$conn = new mysqli($_POST['dbHost'], $_POST['dbUsername'], $_POST['dbPassword'], $_POST['dbName']);
	if ($conn->connect_error) {
		die("Cannot open database");
	}
    foreach ($TempData as $key => $sql) {
		if (!$conn->query($sql)){
	        die($conn->error);
	    }
	}
	$TempData = '';
	if (file_exists($FilePath)) {
		$TempData = '$dbHost = "'. $_POST['dbHost'] . '", $dbName = "' . $_POST['dbName'] . '", $dbUsername = "' . $_POST['dbUsername'] . '", $dbPassword = "'.$_POST['dbPassword'] .'"' ;
		$TempData = str_replace('ARGUMENTS', $TempData, $writeData);
		$configFile = fopen($FilePath, 'a');
			fwrite($configFile, PHP_EOL . $TempData);
		fclose($configFile);
	}
	header('Location: ./finish.php');
}


?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
        <title>Simcrip Inestaller</title>
        <link rel="stylesheet" type="text/css" href="./admin/style.css">
        <meta name="designer" content="AliA_MehR">
        <meta name="author" content="AliA_MehR">
        <meta name="designerUrl" content="http://alia.cf/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
        <link rel="stylesheet" href="./main.css">
        <script src="../theme/js/jquery.min.js"></script>
        <script src="../theme/js/bootstrap.min.js"></script>
</head>
<body class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" id="mainSectionPage">
			<div class="jumbotron" >
		        <article id="articleSection">
		        <?= $textHeader ?>
		            <form method="post" action="">
		            	
		            
						<div class="form-group">
                            <label for="dbHost">Data Base Host</label>
                            <input class="form-control" id="dbHost" name="dbHost" type="text" maxlength="200" placeholder="localhost">

                            <label for="dbName">Data Base Name</label>
                            <input class="form-control" id="dbName" name="dbName" type="text" maxlength="200" placeholder="ssu">

                            <label for="dbUsername">Data Base Username</label>
                            <input class="form-control" id="dbUsername" name="dbUsername" type="text" maxlength="200" placeholder="root">

                            <label for="dbPassword">Data Base Password</label>
                            <input class="form-control" id="dbPassword" name="dbPassword" type="password" maxlength="200">
		                </div>
		        </article>
				<ul class="pager">

					 <li class="previous"><a href="./index.php"><span aria-hidden="true">&larr;</span> Back</a></li>
					 <button type="submit" name="inestall" class="btn btn-success pull-right">Start Inestall</button>
				</ul>
		        <div class="form-group">
		        	
		        </div>

		        </form>
		        <footer>
				    <h6 class="text-center">
		                All rights reserved. Simcrip Inestaller version 0.1
		        	</h6>
		        </footer>
			</div>
		</div>
		
	</div>
</body>
</html>