SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
/*RULE_SQL_RULE*/
SET time_zone = "+00:00";
/*RULE_SQL_RULE*/
CREATE TABLE `configuration` (
  `id_config` tinyint(3) UNSIGNED NOT NULL,
  `changed` datetime NOT NULL,
  `username` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `site_title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `site_description` varchar(150) COLLATE utf8_persian_ci DEFAULT NULL,
  `site_created` datetime NOT NULL,
  `site_about` mediumtext COLLATE utf8_persian_ci,
  `notification` varchar(1000) COLLATE utf8_persian_ci DEFAULT NULL,
  `site_tag` text COLLATE utf8_persian_ci,
  `site_copy` text COLLATE utf8_persian_ci,
  `admin_name` text COLLATE utf8_persian_ci,
  `ip_creator` varchar(40) CHARACTER SET latin1 NOT NULL,
  `ip_changer` varchar(40) CHARACTER SET latin1 NOT NULL,
  `max_up_size` int(10) UNSIGNED DEFAULT NULL,
  `mime_allowed` varchar(20000) COLLATE utf8_persian_ci DEFAULT NULL,
  `upload_location` varchar(300) COLLATE utf8_persian_ci DEFAULT NULL,
  `template` mediumtext COLLATE utf8_persian_ci,
  `message_allowed` tinyint(1) NOT NULL,
  `error_allowed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
/*RULE_SQL_RULE*/
INSERT INTO `configuration` (`id_config`, `changed`, `username`, `password`, `email`, `site_title`, `site_description`, `site_created`, `site_about`, `notification`, `site_tag`, `site_copy`, `admin_name`, `ip_creator`, `ip_changer`, `max_up_size`, `mime_allowed`, `upload_location`, `template`, `message_allowed`, `error_allowed`) VALUES
(1, '2016-11-23 15:39:47', 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'admin@alia.gq', 'Simcript Uploader', 'a free upload center', '2016-03-26 15:09:53', 'This script is free. in version 0.1', 'In the name of Allah', 'uploader,free script,upload center', '', 'AliA_MehR', '::1', '127.0.0.1', 1048576, 'image/png,image/bmp,image/cis-cod,image/gif,image/ief,image/jpeg,image/jpeg,image/jpeg,image/pipeg,image/svg+xml,image/tiff,image/tiff,image/x-cmu-raster,image/x-cmx,image/x-icon,image/x-portable-anymap,image/x-portable-bitmap,image/x-portable-graymap,image/x-portable-pixmap,image/x-rgb,image/x-xbitmap,image/x-xpixmap,image/x-xwindowdump', './uploads/', NULL, 1, 1);
/*RULE_SQL_RULE*/
CREATE TABLE `contacts` (
  `id_message` mediumint(9) NOT NULL,
  `name_sender` varchar(40) COLLATE utf8_persian_ci NOT NULL,
  `email_sender` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `url_sender` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8_persian_ci NOT NULL,
  `sended` datetime NOT NULL,
  `ip_sender` varchar(40) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
/*RULE_SQL_RULE*/
CREATE TABLE `downloaders` (
  `id_downloader` int(10) UNSIGNED NOT NULL,
  `id_file` int(10) UNSIGNED NOT NULL,
  `ip_downloader` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `date_download` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
/*RULE_SQL_RULE*/
CREATE TABLE `file_detiles` (
  `id_file` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `mime` varchar(80) COLLATE utf8_persian_ci NOT NULL,
  `size` varchar(300) COLLATE utf8_persian_ci NOT NULL,
  `proprietary_code` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `uploaded` datetime NOT NULL,
  `ip_uploader` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `total_download` int(11) DEFAULT NULL,
  `last_downloader` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL,
  `download_link` varchar(800) COLLATE utf8_persian_ci NOT NULL,
  `file_location` varchar(800) COLLATE utf8_persian_ci NOT NULL,
  `deleted` datetime DEFAULT NULL,
  `ip_deleter` varchar(50) COLLATE utf8_persian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
/*RULE_SQL_RULE*/
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id_config`);
/*RULE_SQL_RULE*/
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_message`);
/*RULE_SQL_RULE*/
ALTER TABLE `downloaders`
  ADD PRIMARY KEY (`id_downloader`);
/*RULE_SQL_RULE*/
ALTER TABLE `file_detiles`
  ADD PRIMARY KEY (`id_file`),
  ADD UNIQUE KEY `proprietary_code` (`proprietary_code`);
/*RULE_SQL_RULE*/
ALTER TABLE `configuration`
  MODIFY `id_config` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*RULE_SQL_RULE*/
ALTER TABLE `contacts`
  MODIFY `id_message` mediumint(9) NOT NULL AUTO_INCREMENT;
/*RULE_SQL_RULE*/
ALTER TABLE `downloaders`
  MODIFY `id_downloader` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*RULE_SQL_RULE*/
ALTER TABLE `file_detiles`
  MODIFY `id_file` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;