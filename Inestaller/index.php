<?php
require_once('./conf.inc.php');

?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
        <title>Simcrip Inestaller</title>
        <link rel="stylesheet" type="text/css" href="./admin/style.css">
        <meta name="designer" content="AliA_MehR">
        <meta name="author" content="AliA_MehR">
        <meta name="designerUrl" content="http://alia.cf/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
        <link rel="stylesheet" href="./main.css">
        <script src="../theme/js/jquery.min.js"></script>
        <script src="../theme/js/bootstrap.min.js"></script>
</head>
<body class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" id="mainSectionPage">
			<div class="jumbotron" >
		        <article id="articleSection">
		        	<?= $textHeader ?>
						<div class="form-group">
		                    <textarea  class="form-control" rows="8" readonly><?= $textLicense ?></textarea>
		                </div>
		        </article>
		        <nav aria-label="...">
				  <ul class="pager">
				    <li class="next"><a href="./dbinfo.php">Accept <span aria-hidden="true">&rarr;</span></a></li>
				  </ul>
				</nav>
					
		        <footer>
				    <h6 class="text-center">
		                All rights reserved. Simcrip Inestaller version 0.1
		        	</h6>
		        </footer>
			</div>
		</div>
		
	</div>
</body>
</html>