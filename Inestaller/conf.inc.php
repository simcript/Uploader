<?php
$textEndInstall = 'Install Successfully.
click on finish for delete inestaller and run web site application.
Developer : AliA_MehR
Support By E-Mail: alia_mehr@yahoo.com
My gitlab : https://gitlab.com/alia_mehr
My blog : alia-mehr.blog.ir
gitlab : https://gitlab.com/simcript/Uploader
gitlab : https://gitlab.com/simcript/Inestaller
goodluck.
';
$textHeader = '
		        <h2 class="text-center"><strong>Hello</strong></h2>
		        <h3 class="text-center"><strong>Welcome <small>to</small> Simcrip Uploader</strong></h3>';
		        
$textLicense = 'In The name of Allah
MIT License

Copyright (c) [2016-17] [AliA_MehR]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.';
$setupPathName = '../Inestaller';
$FilePath = '../config.inc.php';
$sqlFilePath = './databases/ssu.sql';
$writeData = '
			function dbConnect(ARGUMENTS) {
			    $conn = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
			    if ($conn->connect_error) {
			        die("Cannot open database");
			    }
			    return $conn;
			}
			function deletSetupPath($path) {
				if ($handle=opendir($path)) { 
			       while (false!==($file=readdir($handle))) { 
			         if ($file<>"." AND $file<>"..") { 
			           if (is_file($path."/".$file))  { 
			             @unlink($path."/".$file); 
			             } 
			           if (is_dir($path."/".$file)) { 
			             deletSetupPath($path."/".$file); 
			             @rmdir($path."/".$file); 
			            } 
			          } 
			        } 
			     } 
                 @rmdir($path);
                 @rmdir("./Inestaller");
                 header("Location: /");
			}
			';
