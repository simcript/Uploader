<?php
require_once('./conf.inc.php');
require_once('../config.inc.php');
if (isset($_POST['endInestall'])) {
	deletSetupPath($setupPathName);
}
?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
        <title>Simcrip Inestaller</title>
        <link rel="stylesheet" type="text/css" href="./admin/style.css">
        <meta name="designer" content="AliA_MehR">
        <meta name="author" content="AliA_MehR">
        <meta name="designerUrl" content="http://alia.cf/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
        <link rel="stylesheet" href="./main.css">
        <script src="../theme/js/jquery.min.js"></script>
        <script src="../theme/js/bootstrap.min.js"></script>
</head>
<body class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" id="mainSectionPage">
			<div class="jumbotron" >
		        <article id="articleSection">
		            <?= $textHeader ?>
						<div class="form-group">
		                    <textarea  class="form-control" rows="8" readonly><?= $textEndInstall ?></textarea>
		                </div>
		        </article>
		        <form method="post" action="">
		        	<button type="submit" name="endInestall" class="btn btn-success btn-lg">Finish Installation</button>
		        </form>
		        <footer>
				    <h6 class="text-center">
		                All rights reserved. Simcrip Inestaller version 0.1
		        	</h6>
		        </footer>
			</div>
		</div>
		
	</div>
</body>
</html>