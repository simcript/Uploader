<?php
class SS_Upload {

  protected $_uploaded = array();               // for Array super global variable $_FILES
  protected $_destination;                      // directory default for move files
  protected $_maxSize = 1048576;                      // maximum size file uploaded default is 1048576 Byte or 1 MB
  protected $_messages = array();               // messages status upload
  protected $_permitted = array();   // types MIME files allowed
  protected $_renamed = false;                  // status renamed file
  protected $_filenames = array();
  protected $_minRand = 3078;                   //minimum number for random number default is 3078
  protected $_maxRand = 9730514;                //maximum number for random number default is 9730514
  protected $_sizeFiles = array();
  public function __construct($path) {
	if (!is_dir($path) || !is_writable($path)) {
	  throw new Exception("Error! $path is not available or not writable!");
	}
	$this->_destination = $path;
	$this->_uploaded = $_FILES;
  }

  public function getMaxSize() {
	return number_format($this->_maxSize/1024, 1) . 'kB';
  }

  public function setMaxSize($num) {
	if (!is_numeric($num)) {
	  throw new Exception("Maximum size must be a number.");
	}
	$this->_maxSize = (int) $num;
  }

  public function move($overwrite = false) {
	$field = current($this->_uploaded);
	if (is_array($field['name'])) {
	  foreach ($field['name'] as $number => $filename) {
		// process multiple upload

		$this->_renamed = false;
		$this->processFile($filename, $field['error'][$number], $field['size'][$number], $field['type'][$number], $field['tmp_name'][$number], $overwrite);
	  }
	} else {
	  $this->processFile($field['name'], $field['error'], $field['size'], $field['type'], $field['tmp_name'], $overwrite);
	}
  }

  public function getMessages() {
	return $this->_messages;
  }

    public function getFileSize($filename){
        return $this->_sizeFiles["$filename"];
    }

  protected function checkError($filename, $error) {
	switch ($error) {
	  case 0:
		return true;
	  case 1:
	  case 2:
	    $this->_messages[] = "$filename exceeds maximum size: " . $this->getMaxSize();
		return true;
	  case 3:
		$this->_messages[] = "Error uploading $filename. Please try again.";
		return false;
	  case 4:
		$this->_messages[] = 'No file selected.';
		return false;
	  default:
		$this->_messages[] = "System error uploading $filename. Contact webmaster.";
		return false;
	}
  }

  protected function checkSize($filename, $size) {
	if ($size == 0) {
	  return false;
	} elseif ($size > $this->_maxSize) {
	  $this->_messages[] = "$filename exceeds maximum size: " . $this->getMaxSize();
	  return false;
	} else {
	  return true;
	}
  }

  protected function checkType($filename, $type) {
	if (empty($type)) {
	  return false;
	} elseif (!in_array($type, $this->_permitted)) {
	  $this->_messages[] = "$filename is not a permitted type of file.";
	  return false;
	} else {
	  return true;
	}
  }

    public function setPermittedTypes($types){
        $types = explode(",",$types);
        $this->isValidMime($types);
        $this->_permitted = $types;
    }
    
  public function addPermittedTypes($types) {
	$types = (array) $types;
    $this->isValidMime($types);
	$this->_permitted = array_merge($this->_permitted, $types);
  }

  public function getFilenames() {
	return $this->_filenames;
  }

  public function setMinMaxRand($min,$max) {    //set minimum and maximum number for generate a random number
    if (($min > 0) && ($max > 0) && ($min < $max)) {
        $this->_minRand = $min;
        $this->_maxRand = $max;
    } else {
        $this->_messages[] = "Minimum and maximum numbers for rename file are unknown";
    }
  }

  protected function getRandom() {          //get a random number between $_minRand and $_maxRand
      return (int) mt_rand($this->_minRand,$this->_maxRand);
  }

  protected function isValidMime($types) {
    $alsoValid = array('application/envoy','application/fractals','application/futuresplash','application/hta','application/internet-property-stream','application/mac-binhex40','application/msword','application/msword','application/octet-stream','application/octet-stream','application/octet-stream','application/octet-stream','application/octet-stream','application/octet-stream','application/octet-stream','application/oda','application/olescript','application/pdf','application/pics-rules','application/pkcs10','application/pkix-crl','application/postscript','application/postscript','application/postscript','application/rtf','application/set-payment-initiation','application/set-registration-initiation','application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.ms-excel','application/vnd.ms-outlook','application/vnd.ms-pkicertstore','application/vnd.ms-pkiseccat','application/vnd.ms-pkistl','application/vnd.ms-powerpoint','application/vnd.ms-powerpoint','application/vnd.ms-powerpoint','application/vnd.ms-project','application/vnd.ms-works','application/vnd.ms-works','application/vnd.ms-works','application/vnd.ms-works','application/winhlp','application/x-bcpio','application/x-cdf','application/x-compress','application/x-compressed','application/x-cpio','application/x-csh','application/x-director','application/x-director','application/x-director','application/x-dvi','application/x-gtar','application/x-gzip','application/x-hdf','application/x-internet-signup','application/x-internet-signup','application/x-iphone','application/x-javascript','application/x-latex','application/x-msaccess','application/x-mscardfile','application/x-msclip','application/x-msdownload','application/x-msmediaview','application/x-msmediaview','application/x-msmediaview','application/x-msmetafile','application/x-msmoney','application/x-mspublisher','application/x-msschedule','application/x-msterminal','application/x-mswrite','application/x-netcdf','application/x-netcdf','application/x-perfmon','application/x-perfmon','application/x-perfmon','application/x-perfmon','application/x-perfmon','application/x-pkcs12','application/x-pkcs12','application/x-pkcs7-certificates','application/x-pkcs7-certificates','application/x-pkcs7-certreqresp','application/x-pkcs7-mime','application/x-pkcs7-mime','application/x-pkcs7-signature','application/x-sh','application/x-shar','application/x-shockwave-flash','application/x-stuffit','application/x-sv4cpio','application/x-sv4crc','application/x-tar','application/x-tcl','application/x-tex','application/x-texinfo','application/x-texinfo','application/x-troff','application/x-troff','application/x-troff','application/x-troff-man','application/x-troff-me','application/x-troff-ms','application/x-ustar','application/x-wais-source','application/x-x509-ca-cert','application/x-x509-ca-cert','application/x-x509-ca-cert','application/ynd.ms-pkipko','application/zip','audio/basic','audio/basic','audio/mid','audio/mid','audio/mpeg','audio/x-aiff','audio/x-aiff','audio/x-aiff','audio/x-mpegurl','audio/x-pn-realaudio','audio/x-pn-realaudio','audio/x-wav','image/bmp','image/cis-cod','image/gif','image/ief','image/jpeg','image/jpeg','image/png','image/jpeg','image/pipeg','image/svg+xml','image/tiff','image/tiff','image/x-cmu-raster','image/x-cmx','image/x-icon','image/x-portable-anymap','image/x-portable-bitmap','image/x-portable-graymap','image/x-portable-pixmap','image/x-rgb','image/x-xbitmap','image/x-xpixmap','image/x-xwindowdump','message/rfc822','message/rfc822','message/rfc822','text/css','text/h323','text/html','text/html','text/html','text/iuls','text/plain','text/plain','text/plain','text/plain','text/richtext','text/scriptlet','text/tab-separated-values','text/webviewhtml','text/x-component','text/x-setext','text/x-vcard','video/mpeg','video/mpeg','video/mpeg','video/mpeg','video/mpeg','video/mpeg','video/quicktime','video/quicktime','video/x-la-asf','video/x-la-asf','video/x-ms-asf','video/x-ms-asf','video/x-ms-asf','video/x-msvideo','video/x-sgi-movie','x-world/x-vrml','x-world/x-vrml','x-world/x-vrml','x-world/x-vrml','x-world/x-vrml','x-world/x-vrml');
  	$valid = array_merge($this->_permitted, $alsoValid);
	foreach ($types as $type) {
	  if (!in_array($type, $valid)) {
		throw new Exception("$type is not a permitted MIME type");
	  }
	}
  }

  protected function checkName($name, $overwrite) {
	$nospaces = str_replace(' ', '_', $name);
	if ($nospaces != $name) {
	  $this->_renamed = true;
	}
	if (!$overwrite) {
	  $existing = scandir($this->_destination);
	  if (in_array($nospaces, $existing)) {
		$dot = strrpos($nospaces, '.');
		if ($dot) {
		  $base = substr($nospaces, 0, $dot);
		  $extension = substr($nospaces, $dot);
		} else {
		  $base = $nospaces;
		  $extension = '';
		}
		do {
          $i = $this->getRandom();
		  $nospaces = $base . '_' . $i . $extension;
		} while (in_array($nospaces, $existing));
		$this->_renamed = true;
	  }
	}
	return $nospaces;
  }

  protected function processFile($filename, $error, $size, $type, $tmp_name, $overwrite) {
	$OK = $this->checkError($filename, $error);
	if ($OK) {
	  $sizeOK = $this->checkSize($filename, $size);
	  $typeOK = $this->checkType($filename, $type);
	  if ($sizeOK && $typeOK) {
		$name = $this->checkName($filename, $overwrite);
          $this->_sizeFiles[$name] = $size;                                                           ///SIZE FILE IS SAVE
          // move and rename file to upload directory
		$success = move_uploaded_file($tmp_name, $this->_destination . $name);
		if ($success) {
	      // add the amended filename to the array of filenames
	      $this->_filenames[] = $name;
			$message = "$filename uploaded successfully";
			if ($this->_renamed) {
			  $message .= " and renamed $name";
			}
			$this->_messages[] = $message;
		} else {
		  $this->_messages[] = "Could not upload $filename";
		}
	  }
	}
  }
}

###############################################
############### Class Messaging ###############
###############################################

class SS_Messaging {


}