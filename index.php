<?php
    include('./processor.inc.php');
    $urlHome = getLinks('','');
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset = utf-8 >
        <title><?php echo isset($setAbout["title"]) ? $setAbout["title"] : 'Simcrip Uploader'; ?></title>
        <meta name="description" content="<?php echo isset($setAbout["description"]) ? $setAbout["description"] : 'Simcrip Uploader is a free script for upload center sites'; ?>" />
        <meta name="keywords" content="<?php echo $setAbout["tags"] ; ?>" />
        <meta name="author" content="<?php echo $setAbout["adminName"] ; ?>" /> 
        <meta http-equiv="Designer" content="Brackets">
        <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
        <meta name="copyright" content="Built-in time 2014-03-26 15:09:53 | website : http://alia.cf" />
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" type="text/css" href="./theme/Style/main.css">
        <script lang="javascript" type="text/javascript" src="./theme/js/dragdropUpload.js"></script>
    </head>

    <body>
    <Section id="main">

        <header>

            <nav>
                <ul>
                    <li><a href="./index.php" <?php echo (strstr($_SERVER['PHP_SELF'],'index')) ? 'id="selected"' : '';?>> Home </a></li>
                    <li><a href="./contact.php" <?php echo (strstr($_SERVER['PHP_SELF'],'contact')) ? 'id="selected"' : '';?>> Contact </a></li>
                    <li><a href="./about.php" <?php echo (strstr($_SERVER['PHP_SELF'],'about')) ? 'id="selected"' : '';?>> About </a></li>
                </ul>
            </nav>
            <?php 
                if (isset($setAbout["notification"]) && !empty($setAbout["notification"])){
                    echo '<article id="notifications"><p>' . $setAbout["notification"] . '</p></article>';
                } else {
                    echo '';
                }
            ?>
        </header>
        <form id="formupload" method="post" action="" enctype="multipart/form-data">
            <section id="uploader" ondrop="drop(event)" ondragover="allowDrop(event)">
                <?php if (isset($outPuts)){
                        $total = count($outPuts);
                        for($k=1;$k<=$total;$k++){
                            echo '<div class="output">';
                                echo 'file Name:  ' . "<h5>" . $outPuts[$k]["name"] . "</h5><br>";           
                                echo 'file Size:  ' . "<h5>" . $outPuts[$k]["size"] . " KB</h5><br>";           
                                echo 'information file:  ' . "<h5>" . $outPuts[$k]["info"] . "</h5><br>";           
                                echo 'direct link for download file:  ' . "<h5>" . $outPuts[$k]["link"] . "</h5><br>";           
                                echo 'delete link file:  ' . "<h5>" . $outPuts[$k]["dell"] . "</h5><br>";           
                            echo '</div>';
                        }
                        echo '<div id="reload" class="btn" onclick="Reload(\''. $urlHome .'\')"><h5>New Upload</h5></div>';
                    } else {?>
                    <p>Drag and Drop Files Here to Upload</p>
                    <div id="Browse" class="btn">
                        <input type="file" id="fileInput" name="selectfile[]" multiple onchange="uploadStart()">
                        <h6>Or Select Files to Upload</h6>
                        <input id="flag" name="flag" type="hidden" value="true" onchange="uploadStart()">
                    </div>
                <?php }?>
            </section>
        </form>    
        <footer>
            <p><?php 
                    $startYear = explode("-",$setAbout["created"]);
                    echo (date('Y') == $startYear[0]) ? $startYear[0] : $startYear[0] . '-' . date('y');
                    echo ' ';
                    echo empty($setAbout["copyRight"]) ? '&copy; All rights reserved.' : $setAbout["copyRight"] ; 
                ?> </p>
        </footer>
</Section><!--main-->
</body>
</html>