<?php
include('./config.inc.php');

if (isset($_POST['send']) && ($setAbout["message"] == 1) ){
    require_once ('./config.inc.php');
    $Ips = (string) GetRealIp();
    $conn = dbConnect();
    $sql = 'INSERT INTO contacts (name_sender, email_sender, url_sender, content, ip_sender, sended) VALUES (?, ?, ?, ?, ?, NOW())';
    $stmt = $conn->stmt_init();
    if (strlen(trim($_POST['yourname'])) * strlen(trim($_POST['youremail'])) * strlen(trim($_POST['yourmessage'])) != 0) {
        if ($stmt->prepare($sql)) {
            $stmt->bind_param('sssss', $_POST['yourname'], $_POST['youremail'], $_POST['yourweb'], $_POST['yourmessage'], $Ips);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $OK = "Hooray! Your message was received.";
            }
        }
    }
    else {
        $OK = 'Error! Starred items are mandatory to fill';
    }
} 
?>
<!DOCTYPE HTML>
<html>
<head>
        <meta charset = utf-8 >
        <title><?php echo isset($setAbout["title"]) ? $setAbout["title"] : 'Simcrip Uploader'; ?></title>
        <meta name="description" content="<?php echo isset($setAbout["description"]) ? $setAbout["description"] : 'Simcrip Uploader is a free script for upload center sites'; ?>" />
        <meta name="keywords" content="<?php echo $setAbout["tags"] ; ?>" />
        <meta name="author" content="<?php echo $setAbout["adminName"] ; ?>" /> 
        <meta http-equiv="Designer" content="Brackets">
        <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
        <meta name="copyright" content="Built-in time 2014-03-26 15:09:53 | website : http://alia.cf" />
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" type="text/css" href="./theme/Style/main.css">
        <script lang="javascript" type="text/javascript" src="./theme/js/dragdropUpload.js"></script>
</head>

<body>
      
    
    <Section id="main">

        <header>

            <nav>
                <ul>
                    <li><a href="./index.php" <?php echo (strstr($_SERVER['PHP_SELF'],'index')) ? 'id="selected"' : '';?>> Home </a></li>
                    <li><a href="./contact.php" <?php echo (strstr($_SERVER['PHP_SELF'],'contact')) ? 'id="selected"' : '';?>> Contact </a></li>
                    <li><a href="./about.php" <?php echo (strstr($_SERVER['PHP_SELF'],'about')) ? 'id="selected"' : '';?>> About </a></li>
                </ul>
            </nav>
            <?php 
                if (isset($setAbout["notification"]) && !empty($setAbout["notification"])){
                    echo '<article id="notifications"><p>' . $setAbout["notification"] . '</p></article>';
                } else {
                    echo '';
                }
            ?>
        </header>
        
        <section id="formContact">
            <?php if ($setAbout["message"] == 1) { ?>
            <form action="" method="post" >
                
                <input type="text" class="typeText" id="name" name="yourname" maxlength="30" placeholder="Name">
                
                <input type="email" class="typeText" id="email" name="youremail" maxlength="100" placeholder="Email">
                
                <input type="url" class="typeText" id="web" name="yourweb" maxlength="70" placeholder="Web">
                
                <textarea id="message" name="yourmessage" cols="60" rows="8" placeholder="Message"></textarea>
                
                <input type="submit" id="send" name="send" value="Send Message">
            </form>
            <?php } else { echo '<section id="result"> <p> Oh sorry! There is the possibility to contact us </p></section>' ; }?>
            <?php if (isset($OK)){ ?>
            <section id="result">
                <?php
                         
                 echo "<p>$OK</p>";
                        
                ?>
            </section>
            <?php } ?>
        </section> <!-- formContact -->
        
        <footer>
            <p><?php 
                $startYear = explode("-",$setAbout["created"]);
                echo (date('Y') == $startYear[0]) ? $startYear[0] : $startYear[0] . '-' . date('y');
                echo ' ';
                echo empty($setAbout["copyRight"]) ? '&copy; All rights reserved.' : $setAbout["copyRight"] ; 
            ?> </p>
        </footer>
    </Section><!--main-->
</body>
</html>