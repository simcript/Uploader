<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/29/2016
 * Time: 02:44 PM
 */
function sessionCheck($isExist = false, $redirect = '../login.php', $sessionVariable = 'ssu', $sessionValue = 'Saleh') {
    if (!$isExist){
        if (!isset($_SESSION["$sessionVariable"])) {
            header("Location: $redirect");
            exit;
        }
    } else {
        if (isset($_SESSION["$sessionVariable"])) {
            if ($_SESSION["$sessionVariable"] == "$sessionValue") {
                header("Location: $redirect");
                exit;
        }
        }
    }
}
function logout($redirect = '../index.php'){
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-86400,'/');
    }
    session_destroy();
    header("Location: $redirect");
    exit;
}
function mailer($to, $subject, $content, $siteName, $siteEmail){
    $headers = "From: $siteName<$siteEmail>\r\n";
    $headers .= 'Content-Type: text/plain; charset=utf-8';
    $message = "Name: $siteName\r\n\r\n";
    $message .= "Email: $siteEmail\r\n\r\n";
    $message .= "Comments: $content";
    @$result = mail($to, $subject, $message, $headers);
    return $result;
}