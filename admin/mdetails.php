<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/28/2016
 * Time: 10:58 AM
 */
require_once ('./functions.inc.php');
session_start();
sessionCheck();
include('./theme.inc.php');
if(isset($_GET['id'])) {
    require_once('../config.inc.php');
    $conn = dbConnect();
    $stmt = $conn->stmt_init();
    $sql = 'SELECT sended, ip_sender, name_sender, email_sender, url_sender, content FROM contacts WHERE id_message = ? ';
    if ($stmt->prepare($sql)) {
        $stmt->bind_param('i', $_GET['id']);
        $stmt->bind_result($dateSend, $ipSend, $nameSend, $emailSend, $urlSend, $contentSend);
        $OK = $stmt->execute();
        $stmt->fetch();
        if (!$OK) {
            $error = $stmt->error;
            exit;
        }
    }
}
?>
<!doctype html>
<html>
<head>
    <?php html_head(); ?>
</head>
<body>
<header>
    <?php theme_nav('Messages'); ?>
</header>
<?php
if (strlen($dateSend) == 0) {
    echo isset($error) ? "<p> $error </p>" : "<p>Error! this message not fund</p>";
} else {
?>
    <section id="mainBody" class="container">
        <article id="articleSection">
            <h1 class="text-center"><strong>Details Message</strong></h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form method="post" action="">
                        
                        <div class="row form-group" id="messageinfo">
                            <div class="col-xs-6">
                                <label for="name">Name</label>
                                <input class="form-control" id="name" name="sendname" type="text" maxlength="30" value="<?php echo isset($nameSend) ? htmlentities($nameSend, ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-6">
                                <label for="web">Web</label>
                                <input class="form-control" id="web" name="sendweb" type="text" maxlength="70" value="<?php echo isset($urlSend) ? htmlentities($urlSend, ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="email">Email</label>
                                <input class="form-control" id="email" name="sendemail" type="text" maxlength="100" value="<?php echo isset($emailSend) ? htmlentities($emailSend, ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="message">Message</label>
                                <textarea class="form-control" rows="5" id="message" name="sendmessage"><?php echo isset($contentSend) ? $contentSend :''; ?></textarea>
                            </div>
                            <div class="col-xs-6">
                                <label for="date">Date Send</label>
                                <input class="form-control" id="date" name="senddate" type="text" maxlength="30" value="<?php echo isset($dateSend) ? htmlentities($dateSend, ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-6">
                                <label for="ip">IP</label>
                                <input class="form-control" id="ip" name="sendip" type="text" maxlength="30" value="<?php echo isset($ipSend) ? htmlentities($ipSend, ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
            </div>
            
        </article>
        <footer>
            <?php theme_footer(); ?>
        </footer>
    </section>
</body>
</html>