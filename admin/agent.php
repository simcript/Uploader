<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 04/14/2016
 * Time: 04:25 PM
 */
include('../config.inc.php');
require_once ('./functions.inc.php');
session_start();
sessionCheck();
include('./theme.inc.php');
if (!isset($_POST['subRes'])) {
    if (!isset($_POST['subDel'])) {
        if (isset($_GET['do'])) {
            $conn = dbConnect();
            $stmt = $conn->stmt_init();
            $sql = 'SELECT id_file, file_name, mime, size, proprietary_code, uploaded, ip_uploader, total_download, last_downloader, download_link, file_location, deleted, ip_deleter FROM file_detiles WHERE proprietary_code = ?';
            if ($stmt->prepare($sql)) {
                $stmt->bind_param('s', $_GET['id']);
                $stmt->bind_result($fileId, $fname, $ftype, $fSize, $fPC, $upDate, $ipUp, $totalDL, $lastDL, $linkDL, $locationFile, $delDate, $ipDel);
                if ($stmt->execute() && $stmt->fetch()) {
                    $OK = TRUE;
                } else {
                    $OK = False;
                }
            }
            if (isset($_GET['dlrId'])){
                $conn = dbConnect();
                $stmt = $conn->stmt_init();
                $sql = 'SELECT ip_downloader, date_download FROM downloaders WHERE id_downloader = ?';
                if ($stmt->prepare($sql)) {
                    $stmt->bind_param('s', $_GET['dlrId']);
                    $stmt->bind_result($dlrIp, $dlrDate);
                    $stmt->execute();
                    $stmt->fetch();
                }
            }
        } else {
            header("Location: ./trash.php");
        }
    } else {
        if ($_POST['subDel'] == 'Yes') {
            $conn = dbConnect();
            $stmt = $conn->stmt_init();
            $sql = 'DELETE FROM file_detiles WHERE proprietary_code = ?';
            if ($stmt->prepare($sql)) {
                $stmt->bind_param('s', $_GET['id']);
                if ($stmt->execute()) {
                    $sql = 'DELETE FROM downloaders WHERE id_file = ?';
                    if ($stmt->prepare($sql)) {
                        $stmt->bind_param('i', $_GET['fid']);
                        if ($stmt->execute()) {
                            $failName = "." . $destination . $_GET['fn'];
                            if (file_exists($failName)){
                                unlink($failName);
                            }
                            $errors[] = 'This file removed. <a href="./trash.php">Back to trash</a>';
                            $OK = False;
                        }
                    }
                } else {
                    $errors[] = 'This file not a trash. <a href="./trash.php">Back to trash</a>';
                    $OK = False;
                }
            }
        } else {
            header("Location: ./trash.php");
        }
    }
} else {
    if ($_POST['subRes'] == 'Yes') {
        $conn = dbConnect();
        $stmt = $conn->stmt_init();
        $sql = 'UPDATE file_detiles SET deleted = NULL, ip_deleter = NULL WHERE proprietary_code = ?';
        if ($stmt->prepare($sql)) {
            $stmt->bind_param('s', $_GET['id']);
            if ($stmt->execute()) {
                $errors[] = 'This file restored. <a href="./trash.php">Back to trash</a>';
                $OK = False;
            } else {
                $errors[] = 'This file not a trash. <a href="./trash.php">Back to trash</a>';
                $OK = False;
            }
        }
    } else {
        header("Location: ./trash.php");
    }

}
?>
<!doctype html>
<html>
<head>
    <?php html_head(); ?>
</head>
<body>
<header>
    <?php if ($_GET['do'] === 'details') {
            theme_nav('Files');
        } else {
            theme_nav('Trash');   
        }
    ?>
</header>
<?php

    if (!$OK){ ?>
        <section id="mainBody" class="container">
            <article id="articleSection">
                <div class="alert alert-info ?>" role="alert">
                <?php
                foreach ($errors as $err => $value) {
                    echo "<strong>Note:</strong> $value";
                }
                ?>             
                </div>
            </article>
        </section>
    <?php
        exit;
    }
    switch ($_GET['do']) {
            case "details" :
                $tempHeaderText = "<p>Details File $fname</p>";
            break;
            case "restore" :
                if (!empty($delDate)) {
                    $tempNote['text'] = "Do you want to Restore this file ($fname)?";
                    $tempNote['alertStatus'] = "success";
                    $tempNote['submitName'] = "subRes";
                    $tempHeaderText = "<p>Restore File $fname</p>";
                } else {
                    echo 'This file not a trash. <a href="./trash.php">Back to trash</a>';
                    exit;
                }
            break;
            case "delete" :
                if (!empty($delDate)) {
                    $tempNote['text'] = "Do you want to Delete this file ($fname)?";
                    $tempNote['alertStatus'] = "danger";
                    $tempNote['submitName'] = "subDel";
                    $tempHeaderText = "<p>Delete File $fname</p>";
                } else {
                    echo 'This file not a trash. <a href="./trash.php">Back to trash</a>';
                    exit;
                }
            break;
    }
?>

<section id="mainBody" class="container">
        <article id="articleSection">
            <h1 class="text-center"><strong><?= $tempHeaderText ?></strong></h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                        <div class="row form-group" id="messageinfo">
                            <div class="col-xs-6">
                                <label for="name">file name</label>
                                <input class="form-control" id="name" name="name" type="text" value="<?= $fname; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="MIME">file MIME</label>
                                <input class="form-control" id="MIME" name="MIME" type="text" value="<?= $ftype; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="Size">file Size</label>
                                <input class="form-control" id="Size" name="Size" type="text" value="<?= $fSize; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="Proprietary">file Proprietary code</label>
                                <input class="form-control" id="Proprietary" name="Proprietary" type="text" value="<?= $fPC; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="upDate">Upload date</label>
                                <input class="form-control" id="upDate" name="upDate" type="text" value="<?= $upDate; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="ipUp">IP uploader</label>
                                <input class="form-control" id="ipUp" name="ipUp" type="text" value="<?= $ipUp; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="totalDL">Total Download</label>
                                <input class="form-control" id="totalDL" name="totalDL" type="text" value="<?= ($totalDL < 1) ? 0 : $totalDL ; ?>" readonly>
                            </div>
                              
                            <div class="col-xs-6">
                                <?php
                                    if(!isset($dlrIp)){
                                        if ($lastDL < 1) {
                                            echo '<label for="lastDL">Last Downloader</label>';
                                            $lastDL = 0;
                                        } else {
                                            echo '<label for="lastDL"><a href=' . './agent.php?do=' . $_GET['do'] . '&id=' . $_GET['id'] . '&fid=' . $fileId . "&fn=" . $fname . '&dlrId=' . $lastDL .">Last Downloader</a></lable>";
                                        }
                                        $tempLastDLData = ($lastDL < 1) ? '' : 'id: '. $lastDL ;
                                    } else {
                                        echo '<label for="lastDL">Last Downloader</label>';
                                        $tempLastDLData = 'IP:' . $dlrIp . 'in Date:' . $dlrDate;
                                    }
                                ?>
                                <input class="form-control" id="lastDL" name="lastDL" type="text" value= "<?= $tempLastDLData ?>" readonly>
                            </div>

                            <div class="col-xs-6">
                                <label for="linkDL">Download link</label>
                                <input class="form-control" id="linkDL" name="linkDL" type="text" value="<?= $linkDL; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="fileloc">file Location</label>
                                <input class="form-control" id="fileloc" name="fileloc" type="text" value="<?= $locationFile; ?>" readonly>
                            </div>
                            <?php if(!empty($delDate)){ ?>
                            <div class="col-xs-6">
                                <label for="delDate">Delete date</label>
                                <input class="form-control" id="delDate" name="delDate" type="text" value="<?= $delDate; ?>" readonly>
                            </div>
                            <?php } if(!empty($ipDel)){ ?>
                            <div class="col-xs-6">
                                <label for="ipDel">IP Deleter</label>
                                <input class="form-control" id="ipDel" name="ipDel" type="text" value="<?= $ipDel; ?>" readonly>
                            </div>
                            <?php } ?>
                            
                        </div>
                        <div class="row form-group" id="showDLs">
                            <div class="col-xs-12">
                                <?= ($totalDL < 1) ? '' : '<a class="btn btn-primary" role="button" href=' . './downloaders.php?fn=' . $fname . '&fid=' . $fileId . '> Show All Downloader this file </a>' ?>
                            </div>
                        </div>
                    <form method="post" action="">

                        <?php if(isset($tempNote)) { ?>
                        <div class="form-group" id="buttons">
                            <div class="alert alert-<?= $tempNote['alertStatus'] ?>" role="alert">
                                <strong>Note:</strong> <?=  $tempNote['text'] ?>
                                <input type="submit" class="btn btn-<?= $tempNote['alertStatus'] ?>" name="<?= $tempNote['submitName'] ?>" value="Yes">
                                <input type="submit" class="btn btn-default" name="<?= $tempNote['submitName'] ?>" value="No">
                            </div>
                        </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
            
        </article>
        <footer>
            <?php theme_footer(); ?>
        </footer>
    </section>
</body>
</html>