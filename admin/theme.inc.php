<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 04/16/2016
 * Time: 12:34 PM
 */
function theme_nav($activeLink = 'Dashboard'){
    echo '
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <a class="navbar-brand" href="../index.php"><img src="../logo" alt="Simcript/Uploader" width="25" height="25"></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
    ';
                        echo '<li '.($activeLink==='Dashboard' ? 'class=active' : '').'><a href="./index.php">Dashboard</a></li>';
                        echo '<li '.($activeLink==='Files' ? 'class=active' : '').'><a href="./files.php">Files</a></li>';
                        echo '<li '.($activeLink==='Trash' ? 'class=active' : '').'><a href="./trash.php">Trash bin</a></li>';
                        echo '<li '.($activeLink==='Messages' ? 'class=active' : '').'><a href="./inbox.php">Messages</a></li>';
                        echo '<li '.($activeLink==='Setting' ? 'class=active' : '').'><a href="./settings.php">Setting</a></li>';
    echo '
                        <li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm">Exit</a></li>
                    </ul>   
                </div>
                
            </div>
        </nav>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Exit Admin</h4>
                  </div>
                  <div class="modal-body">
                    Are you do Exit?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                    <a href="../login.php?logout=yes" class="btn btn-danger">Yes, Exit</a>
                  </div>
                
            </div>
          </div>
        </div>
    ';
}

function theme_footer($activeLink = null){
    echo '
        <h6 class="text-center">
                All rights reserved. <a href="https://gitlab.com/simcript/Uploader" target="_blank">Simcrip/Uploader</a> version 1.0
        </h6>
    ';
}

function theme_nav_index(){
    echo '
        <nav>
            <ul>
                <li>
                    <a href="./index.php">Home</a>
                </li>
                <li>
                    <a href="./admin/index.php">Admin</a>
                </li>
                <li>
                    <a href="./message.php">Contact</a>
                </li>
                <li>
                    <a href="./about.php">About</a>
                </li>
            </ul>
        </nav>
    ';
}

function html_head_index(){
    echo '
        <meta charset="utf-8">
        <title>Simcrip Uploader</title>
        <link rel="stylesheet" type="text/css" href="./admin/style.css">
        <meta name="designer" content="AliA_MehR">
        <meta name="author" content="AliA_MehR">
        <meta name="designerUrl" content="http://alia.cf/">
        <meta name="description" content="Simcrip Uploader is free upload center">
        <meta name="keywords" content="Simcrip, Updoder, uploadcenter,">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Bree+Serif|Crimson+Text|Signika" rel="stylesheet" type="text/css">
    ';
}

function html_head(){
    echo '
        <meta charset="utf-8">
        <title>Simcrip Uploader</title>
        <link rel="stylesheet" type="text/css" href="./admin/style.css">
        <meta name="designer" content="AliA_MehR">
        <meta name="author" content="AliA_MehR">
        <meta name="designerUrl" content="http://alia.cf/">
        <meta name="description" content="Simcrip Uploader is free upload center">
        <meta name="keywords" content="Simcrip, Updoder, uploadcenter,">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
        <link rel="stylesheet" href="../theme/css/style.css">
        <script src="../theme/js/jquery.min.js"></script>
        <script src="../theme/js/bootstrap.min.js"></script>
    ';
}


