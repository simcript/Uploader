<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 04/16/2016
 * Time: 01:38 PM
 */
include ('../config.inc.php');
require_once ('./functions.inc.php');
session_start();
sessionCheck();
include('./theme.inc.php');
if(isset($_GET['fid']) && isset($_GET['fn'])) {
    $conn = dbConnect();
    $stmt = $conn->stmt_init();
    $sql = 'SELECT id_downloader, ip_downloader, date_download FROM downloaders WHERE id_file = ?';
    $stmt->prepare($sql);
    $stmt->bind_param('i', $_GET['fid']);
    $stmt->bind_result($dlrId,$dlrIP,$dlDate);
    $stmt->execute();
    $stmt->store_result();
    $numRows = $stmt->num_rows;
    if ($numRows<1) {
        $error = 'This file is not downloaded';
    } else {
        $result = $conn->query($sql);
    }
} else {
    $error = "Please select a file.";
}
?>
<!doctype html>
<html>
<head>
    <?php html_head(); ?>
</head>
<body>
<header>
    <?php theme_nav('Files'); ?>
</header>
    <section id="mainBody" class="container">
    <?php
        if (isset($error)) {
            echo '
            <section id="mainBody" class="container">
                    <article id="articleSection">
                        <div class="alert alert-info ?>" role="alert">';
                            echo "<strong>Note:</strong> $error";
                            echo '  
                        </div>
                    </article>
            </section>';
        } else {
    ?>
        <article id="articleSection">
            <h1 class="text-center"><strong>Downloader List</strong></h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="tabale-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Filename</th>
                                    <th>Downloader ip</th>
                                    <th>Download date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; while ($row = $stmt->fetch()) { ?>
                                <tr>
                                    <td><?php echo $i++ ?></td>
                                    <td><?php echo $dlrId; ?></td>
                                    <td><?php echo $_GET['fn']; ?></td>
                                    <td><?php echo $dlrIP; ?></td>
                                    <td><?php echo $dlDate; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </article>
        <?php } ?>
        <footer>
            <?php theme_footer(); ?>
        </footer>
    </section>
</body>
</html>