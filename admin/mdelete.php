<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/28/2016
 * Time: 10:58 AM
 */
require_once ('./functions.inc.php');
session_start();
sessionCheck();
include('./theme.inc.php');
if(isset($_GET['id'])) {
    require_once('../config.inc.php');
    $conn = dbConnect();
    $stmt = $conn->stmt_init();
    $sql = 'SELECT sended, ip_sender, name_sender, email_sender, url_sender, content FROM contacts WHERE id_message = ? ';
    if ($stmt->prepare($sql)) {
        $stmt->bind_param('i', $_GET['id']);
        $stmt->bind_result($dateSend, $ipSend, $nameSend, $emailSend, $urlSend, $contentSend);
        $OK = $stmt->execute();
        $stmt->fetch();
        if (!$OK) {
            $error = $stmt->error;
            exit;
        }
    }
}
if (isset($_POST['deleteConfirm'])){
    $sql = 'DELETE FROM contacts WHERE id_message = ?';
    if ($stmt->prepare($sql)) {
        $stmt->bind_param('i', $_GET['id']);
        $OKdelete = $stmt->execute();
        if ($OKdelete) {
            $error = 'Message deleted.';

        } else {
            $error = 'There was a problem deleting the message.';
        }
    }
}
if (isset($_POST['deleteCancel'])){
    header('Location: ./inbox.php');
}
?>
<!doctype html>
<html>
<head>
    <?php html_head(); ?>
</head>
<body>
<header>
    <?php theme_nav('Messages'); ?>
</header>
<?php
if (isset($error) || strlen($dateSend) == 0) {
    echo '<section id="mainBody" class="container"><article id="articleSection"><div class="alert alert-info ?>" role="alert">';
    echo strlen($dateSend) == 0 ? 'Error! this message not fund' : $error;
    echo '<a href="./inbox.php"> Back to inbox</a>';
    echo '</div></article></section>';
} else {
    ?>
    <section id="mainBody" class="container">
        <article id="articleSection">
            <h1 class="text-center"><strong>Delete Message</strong></h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form method="post" action="">
                        
                        <div class="row form-group" id="messageinfo">
                            <div class="col-xs-6">
                                <label for="name">Name</label>
                                <input class="form-control" id="name" name="name" type="text" value="<?php echo isset($nameSend) ? htmlentities($nameSend, ENT_COMPAT, 'utf-8'):''; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="web">Web</label>
                                <input class="form-control" id="web" name="web" type="text" value="<?php echo isset($urlSend) ? htmlentities($urlSend, ENT_COMPAT, 'utf-8'):''; ?>" readonly>
                            </div>
                            <div class="col-xs-12">
                                <label for="email">Email</label>
                                <input class="form-control" id="email" name="description" type="text" value="<?php echo isset($emailSend) ? htmlentities($emailSend, ENT_COMPAT, 'utf-8'):''; ?>" readonly>
                            </div>
                            <div class="col-xs-12">
                                <label for="message">Message</label>
                                <textarea class="form-control" rows="5" id="message" name="message" readonly><?php echo isset($contentSend) ? $contentSend :''; ?></textarea>
                            </div>
                            <div class="col-xs-6">
                                <label for="datesend">Date Send</label>
                                <input class="form-control" id="datesend" name="datesend" type="text" value="<?php echo isset($dateSend) ? htmlentities($dateSend, ENT_COMPAT, 'utf-8'):''; ?>" readonly>
                            </div>
                            <div class="col-xs-6">
                                <label for="ip">IP</label>
                                <input class="form-control" id="ip" name="ip" type="text" value="<?php echo isset($ipSend) ? htmlentities($ipSend, ENT_COMPAT, 'utf-8'):''; ?>" readonly>
                            </div>

                        </div>
                        <div class="alert alert-danger" role="alert">
                                <strong>Note:</strong> Please confirm that you want to delete the following item. This action cannot be undone. 
                        </div>
                        <div class="row form-group" id="buttons">
                            <div class="col-xs-3 col-xs-offset-4">
                                <input type="submit" class="btn btn-danger" name="deleteConfirm" value="Confirm Deletion">
                            </div>
                            <div class="col-xs-3">
                                <input type="submit" class="btn btn-default" name="deleteCancel" value="Cancel">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
            </div>
            
        </article>
        <footer>
            <?php theme_footer(); ?>
        </footer>
    </section>
</body>
</html>