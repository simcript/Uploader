<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 04/01/2016
 * Time: 08:32 PM
 */
include ('../config.inc.php');
require_once ('./functions.inc.php');
session_start();
sessionCheck();
include('./theme.inc.php');
$conn = dbConnect();
$sql = 'SELECT * FROM file_detiles WHERE deleted IS NULL';
$testResult = $conn->query($sql);
$testResult = $testResult->fetch_row();
$status = $testResult[0] > 0 ? true : false;
if (!$status) {
    $error = 'There are no file';
} else {
    $stmt = $conn->stmt_init();
    $sql = 'SELECT file_name, size, proprietary_code, uploaded, download_link FROM file_detiles WHERE deleted IS NULL';
}
?>
<!doctype html>
<html>
<head>
    <?php html_head(); ?>
</head>
<body>
<header>
    <?php theme_nav('Files'); ?>
</header>
    <section id="mainBody" class="container">
    <?php
        if (isset($error)) {
            echo '
            <section id="mainBody" class="container">
                    <article id="articleSection">
                        <div class="alert alert-info ?>" role="alert">';
                            echo "<strong>Note:</strong> $error";
                            echo '  
                        </div>
                    </article>
            </section>';
        } else {
    ?>
        <article id="articleSection">
            <h1 class="text-center"><strong>Files List</strong></h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="tabale-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th>Uploaded at</th>
                                    <th>Oprations</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; foreach($conn->query($sql) as $row) { ?>
                                <tr>
                                    <td><?php echo $i++ ?></td>
                                    <td><?php echo $row['file_name']; ?></td>
                                    <td><?php echo $row['size']; ?></td>
                                    <td><?php echo $row['uploaded']; ?></td>
                                    <td><a href="./agent.php?do=details&id=<?php echo $row['proprietary_code']; ?>">Details</a>&nbsp;
                                    <a href="<?php echo $row['download_link']; ?>">Download</a>&nbsp;
                                    <a href="../dl.php?pc=<?php echo $row['proprietary_code']; ?>" target = "_blank">Trashed</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </article>
        <?php } ?>
        <footer>
            <?php theme_footer(); ?>
        </footer>
    </section>
</body>
</html>