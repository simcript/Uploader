<?php
/**
 * Created by PhpStorm.
 * User: AliA_MehR
 * Date: 03/24/2016
 * Time: 05:50 PM
 */
require_once ('./functions.inc.php');
session_start();
sessionCheck();
include('./theme.inc.php');
global $Errors ;
require_once ('../config.inc.php');
$Ips = (string) GetRealIp();
$conn = dbConnect();
$getTotal = 'SELECT COUNT(*) FROM configuration';
$total = $conn->query($getTotal);
$allRow = $total->fetch_row();
$status = $allRow[0] > 0 ? true : false;  // true is update database & false is insert to database
if ($status) {
    if (isset($_POST['save'])){
        $stmt = $conn->stmt_init();
        $sql = 'SELECT id_config, username, password, email FROM configuration';
        if ($stmt->prepare($sql)){
            $stmt->bind_result($idConfig, $tmpUsername, $tmpPass, $tmpEmail);
            $stmt->execute();
            $stmt->fetch();
        }
        $sql = 'UPDATE configuration SET site_title = ?, site_description = ?, site_tag = ?, site_copy = ?, admin_name = ?, site_about = ?, notification = ?, username = ?, password = ?, email = ?, upload_location = ?, max_up_size = ?, mime_allowed = ?, message_allowed = ?, error_allowed = ?, ip_changer = ?, changed  = NOW()
            WHERE id_config = ?';
        if ($stmt->prepare($sql) && analysFilds($tmpUsername, $tmpPass, $tmpEmail)) {
            $stmt->bind_param('sssssssssssisiisi', $_POST['titleSite'], $_POST['descriptionSite'], $_POST['keywordsSite'], $_POST['copyrightSite'], $_POST['aName'], $_POST['aboutSite'], $_POST['notificationSite'], $_POST['usernameSite'], $_POST['passwordSite'], $_POST['emailSite'], $_POST['locationSite'], $_POST['maxSizeSite'], $_POST['mimeSite'], $_POST['disableMessageSite'], $_POST['disableErrorSite'], $Ips, $idConfig);
            $stmt->execute();
        } else {
            $errors[] = 'Error! Do not save changes';
        }
    }
    $sql = 'SELECT * FROM configuration';
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
} else {
    if (isset($_POST['save'])){
        $stmt = $conn->stmt_init();
        $sql = 'INSERT INTO configuration
          (site_title, site_description, site_tag, site_copy, admin_name, site_about, notification, username, password, email, upload_location, max_up_size, mime_allowed, message_allowed, error_allowed, ip_changer, ip_creator, changed, site_created, template)
          VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW(), NULL)';
        $OK = false;
        if ($stmt->prepare($sql)) {
            if ($_POST['disableErrorSite'] != 1) {
                $_POST['disableErrorSite'] = 0;
            }
            if ($_POST['disableMessageSite'] != 1) {
                $_POST['disableMessageSite'] = 0;
            }
            $stmt->bind_param('sssssssssssisiiss', $_POST['titleSite'], $_POST['descriptionSite'], $_POST['keywordsSite'], $_POST['copyrightSite'], $_POST['aName'], $_POST['aboutSite'], $_POST['notificationSite'], $_POST['usernameSite'], $_POST['passwordSite'], $_POST['emailSite'], $_POST['locationSite'], $_POST['maxSizeSite'], $_POST['mimeSite'], $_POST['disableMessageSite'], $_POST['disableErrorSite'], $Ips, $Ips);
            $stmt->execute();
            if ($stmt->affected_rows > 0){
                $OK = true;
            }
        }
        if($OK) {
            header('Location: ./index.php');
            exit;
        } else {
            $errors[] = $stmt->error;
        }
    }
}
function analysFilds($tmpUsername, $tmpPass, $tmpEmail){
    if (strlen($_POST['usernameSite']) < 5) {
        $Errors[] = "Username must be at least 5 characters.";
        return false;
    }
    if ($_POST['usernameSite'] != $tmpUsername){
       $m = mailer($_POST['emailSite'],'Changes login' . $_POST['titleSite'] . ' details','New Username = ' . $_POST['usernameSite'],$_POST['titleSite'],$_POST['emailSite']);
        if (!$m){
            $Errors[] = "Send e-mail having problems, please contact your host supports the site Simcrip report.";
        }
    }
    if (strlen(trim($_POST['passwordSite'])) == 0) {
        $errors[] = "Your password was not changed";
        if (isset($tmpPass)) {
            $_POST['passwordSite'] = $tmpPass ;
        } else {
            return false ;
        }
    } else {
        $m = mailer($_POST['emailSite'],'Changes login' . $_POST['titleSite'] . ' details','New Password = ' . $_POST['passwordSite'],$_POST['titleSite'],$_POST['emailSite']);
        if (!$m){
            $Errors[] = "Send e-mail having problems, please contact your host supports the site Simcrip report.";
        }
        $_POST['passwordSite'] = sha1($_POST['passwordSite']);
    }
    if ($_POST['emailSite'] != $tmpEmail) {
        $m = mailer($_POST['emailSite'],'Changes login' . $_POST['titleSite'] . ' details','New Email = ' . $_POST['emailSite'],$_POST['titleSite'],$_POST['emailSite']);
        if (!$m){
            $Errors[] = "Send e-mail having problems, please contact your host supports the site Simcrip report.";
        }
    }
    return true;
}

?>
<!doctype html>
<html>
<head>
    <?php html_head(); ?>
</head>
<body>
    <header>
        <?php theme_nav('Setting'); ?>
    </header>
    <?php
     if (isset($errors)) {
         foreach($errors as $error) {
            echo "<p>Error: $error </p>";
         }
     }
    ?>

    <section id="mainBody" class="container">
        <article id="articleSection">
            <h1 class="text-center"><strong>Setting Script</strong></h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form method="post" action="">
                        
                        <div class="row form-group" id="generalinfo">
                        <h3><em><small>General information about the site</small></em></h3>
                            <div class="col-xs-12">
                                <label for="title">Title</label>
                                <input class="form-control" id="title" name="titleSite" type="text" value="<?php echo $status && isset($row) ? htmlentities($row['site_title'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="description">Description</label>
                                <input class="form-control" id="description" name="descriptionSite" type="text" maxlength="150" value="<?php echo $status && isset($row) ? htmlentities($row['site_description'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="keywords">Keywords</label>
                                <input class="form-control" id="keywords" name="keywordsSite" type="text" maxlength="200" value="<?php echo $status && isset($row) ? htmlentities($row['site_tag'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="copyright">Copyright</label>
                                <input class="form-control" id="copyright" name="copyrightSite" type="text" maxlength="70" value="<?php echo $status && isset($row) ? htmlentities($row['site_copy'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="aName">Admin Name</label>
                                <input class="form-control" id="aName" name="aName" type="text" maxlength="20" value="<?php echo $status && isset($row) ? htmlentities($row['admin_name'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="about">About</label>
                                <textarea class="form-control" rows="5" id="about" name="aboutSite"><?php echo $status && isset($row) ? htmlentities($row['site_about'], ENT_COMPAT, 'utf-8'):''; ?></textarea>
                            </div>
                            <div class="col-xs-12">
                                <label for="notification">Notification</label>
                                <textarea class="form-control" rows="5" id="notification" name="notificationSite"><?php echo $status && isset($row) ? htmlentities($row['notification'], ENT_COMPAT, 'utf-8'):''; ?></textarea>
                            </div>
                        </div>

                        <div class="row form-group" id="Confidentialinfo">
                        <h3><em><small>Confidential information to enter the site</small></em></h3>
                            <div class="col-xs-12">
                                <label for="username">Username</label>
                                <input class="form-control" id="username" name="usernameSite" type="text" maxlength="20" value="<?php echo $status && isset($row) ? htmlentities($row['username'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="pass">Password</label>
                                <input class="form-control" id="pass" name="passwordSite" type="password" maxlength="40">
                            </div>
                            <div class="col-xs-12">
                                <label for="mailadd">Email</label>
                                <input class="form-control" id="mailadd" name="emailSite" type="text" maxlength="100" value="<?php echo $status && isset($row) ? htmlentities($row['email'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                        </div>

                        <div class="row form-group" id="uploadsett">
                        <h3><em><small>Upload settings</small></em></h3>
                            <div class="col-xs-12">
                                <label for="loc">Location</label>
                                <input class="form-control" id="loc" name="locationSite" type="text" maxlength="300" value="<?php echo $status && isset($row) ? htmlentities($row['upload_location'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="max">Maximum size</label>
                                <input class="form-control" id="max" name="maxSizeSite" type="text" min="0" max="4000000000" value="<?php echo $status && isset($row) ? htmlentities($row['max_up_size'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="col-xs-12">
                                <label for="mime">MIMEs allowed</label>
                                <input class="form-control" id="mime" name="mimeSite" type="text"  maxlength="20000" value="<?php echo $status && isset($row) ? htmlentities($row['mime_allowed'], ENT_COMPAT, 'utf-8'):''; ?>">
                            </div>
                            <div class="checkbox col-xs-12">
                                <label><input type="checkbox" name="disableMessageSite" value="1"<?php if ($status && isset($row)){ $checkStatus = $row['message_allowed'] == 1 ? ' checked="checked"' : ''; echo $checkStatus;} ?>>Active message</label>
                                <i data-toggle="tooltip" title="Do you want to receive Message?" class="glyphicon glyphicon-question-sign"></i>
                            </div>
                            <div class="checkbox col-xs-12">
                                <input type="hidden" name="disableErrorSite" value="1"<?php if ($status && isset($row)){ $checkStatus = $row['error_allowed'] == 1 ? ' checked="checked"' : ''; echo $checkStatus;} ?>>
                            </div>

                            
                            
                        </div>
                        <div class="alert alert-warning" role="alert">
                            <strong>Note:</strong> If you record changes, your IP will be recorded
                        </div>


                        <div class="row form-group" id="buttons">
                            <div class="col-xs-3 col-xs-offset-4">
                                <input type="submit" class="btn btn-success" name="save" value="Save">
                            </div>
                            <div class="col-xs-3">
                                <input type="submit" class="btn btn-primary" name="commander" value="Default">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </article>
        <footer>
            <?php theme_footer(); ?>
        </footer>
    </section>
</body>
</html>